module TRS.Types (Term (..), Subst, Rule, TRS, Position, showRule, showTRS) where

import Data.List (intercalate)

data Term
    = Var String
    | Con String
    | App Term Term
    | Nf Term
    deriving stock (Eq)
type Subst = [(String, Term)]
type Rule = (Term, Term)
type TRS = [Rule]
type Position = [Int]

showAppNum :: Term -> String
showAppNum whole = loop 0 whole
  where
    loop :: Integer -> Term -> String
    loop n (Con "0") = show n
    loop n (App (Con "s") t) = loop (n + 1) t
    loop n (App (Nf (Con "s")) t) = loop (n + 1) t
    loop n (Nf t) = "s^{" ++ show n ++ "}!" ++ loop 0 t ++ "¡"
    loop _n _t = showAppNorm whole

showAppList :: Term -> String
showAppList whole = loop [] whole
  where
    loop :: [Term] -> Term -> String
    loop l (Con "nil") = "[" ++ intercalate "," (show <$> reverse l) ++ "]"
    loop l (App (App (Con "cons") x) t) = loop (x : l) t
    loop l (App (App (Nf (Con "cons")) x) t) = loop (x : l) t
    loop l (App (Nf (App (Nf (Con "cons")) x)) t) = loop (x : l) t
    loop l (Nf t) = loop l t
    loop _n _t = showAppNorm whole

showAppNorm :: Term -> String
showAppNorm (App f t) = show f ++ " (" ++ show t ++ ")"
showAppNorm t = show t

showRule :: Rule -> String
showRule (l, r) = show l ++ " -> " ++ show r

showTRS :: TRS -> String
showTRS trs = unlines [showRule rule | rule <- trs]

instance Show Term where
    show (Var x) = x
    show (Con "nil") = "[]"
    show (Con c) = c
    show (Nf t) = "!" ++ show t ++ "¡"
    show whole@(App (Con "s") _t) = showAppNum whole
    show whole@(App (App (Con "cons") _x) _t) = showAppList whole
    show whole@(App _f _t) = showAppNorm whole
