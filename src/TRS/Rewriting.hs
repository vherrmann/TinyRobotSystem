module TRS.Rewriting (substitute, match, rewrite, nf) where

import Control.Applicative (asum)
import Control.Monad (guard)
import Data.Bifunctor (Bifunctor (second))
import Data.Functor ((<&>))
import Data.List (group, nub)
import Data.Maybe (fromMaybe)
import Debug.Trace (trace, traceShow, traceShowId)
import PyF (fmt)
import TRS.Types

-- substitute t σ = tσ
substitute :: Term -> Subst -> Term
substitute term@(Var str) σ = fromMaybe term (lookup str σ)
substitute term@(Con _) _ = term
substitute term@(Nf _) _ = term
substitute (App f t) σ = App (substitute f σ) (substitute t σ)

-- match s t = Just σ if sσ = t
--           = Nothing if sσ = t for no σ
match :: Term -> Term -> Maybe Subst
match (Nf t1) t2 = match t1 t2
match t1 (Nf t2) = do
  σ <- match t1 t2
  pure $ second Nf <$> σ
match (Var name) t = Just [(name, t)]
match (Con name1) (Con name2) = [] <$ guard (name1 == name2)
match (Con _) _ = Nothing
match (App f1 t1) (App f2 t2) =
  do
    σf <- match f1 f2
    σt <- match t1 t2
    let σ = nub (σf ++ σt)

    -- check if variables have differing substitutions
    guard $ noDups (fst <$> σ)

    pure σ
 where
  noDups :: (Eq a) => [a] -> Bool
  noDups = all ((1 ==) . length) . group
match (App _ _) _ = Nothing

-- rewrite R s = Just t  if s ->_R t
--             = Nothing if s is a normal form wrt R
rewrite :: TRS -> Term -> Term
rewrite trs s =
  case s of
    Var _ -> rewriteAtRoot s
    Con _ -> rewriteAtRoot s
    Nf _ -> s
    App f t ->
      -- we are rewriting from inside to outside
      let rwF = rewrite trs f
          rwT = rewrite trs t
       in case rwF of
            Nf _ -> case rwT of
              Nf _ -> rewriteAtRoot (App rwF rwT)
              _ -> App rwF rwT
            _ -> App rwF t
 where
  rewriteAtRoot :: Term -> Term
  rewriteAtRoot s = fromMaybe (Nf (removeNfs s)) (tryRewrite trs s)

  removeNfs :: Term -> Term
  removeNfs s = case s of
    Var _ -> s
    Con _ -> s
    Nf s' -> s'
    App f t -> App (removeNfs f) (removeNfs t)

  tryRewrite :: TRS -> Term -> Maybe Term
  tryRewrite trs s =
    asum
      $ trs
      <&> \rule -> do
        let (l, r) = rule
        σ <- match l s
        pure (substitute r σ)

-- nf R s = t, where t is a normal form of s (i.e., s ->^! t)
nf :: TRS -> Term -> Term
nf _trs (Nf t) = t
nf trs s = nf trs (rewrite trs s)
