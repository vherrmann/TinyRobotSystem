{
  description = "TinyRobotSystem";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };
  outputs = inputs@{ self, nixpkgs, flake-utils, ... }:
    let name = "TinyRobotSystem";
    in flake-utils.lib.eachSystem [ "x86_64-linux" "x86_64-darwin" ] (system:
      let
        overlays = [ ];
        pkgs = import nixpkgs {
          inherit system overlays;
          config.allowBroken = false;
        };
        compilerVersion = "ghc94";
        compiler = pkgs.haskell.packages."${compilerVersion}";
        nativeBuildInputsHs = (with compiler; [
          cabal-install
          hlint
          haskell-language-server
          fourmolu
        ]) ++ (with pkgs; [ nixpkgs-fmt treefmt ]);
        projectHs = returnShellEnv:
          compiler.developPackage {
            inherit returnShellEnv;
            name = name;
            root = ./.;
            withHoogle = true;
            modifier = drv:
              pkgs.haskell.lib.addBuildTools drv nativeBuildInputsHs;
          };
      in {
        # Used by `nix build` & `nix run` (prod exe)
        defaultPackage = (projectHs false);

        # `nix develop`
        devShell = (projectHs true);
      });
}
