module Main where

import System.Environment (getArgs)
import TRS.Parser (readTRSFile)
import TRS.Rewriting (nf)
import TRS.Types

main :: IO ()
main = do
    args <- getArgs
    case args of
        [] -> fail "Please provide a file to run."
        [file] -> do
            codeF <- readTRSFile file
            trs <- either (fail . show) pure codeF
            mainFn <-
                maybe (fail "Main Fn is missing") pure
                    $ lookup (Con "main") trs
            print $ nf trs mainFn
        _ -> fail "Too many arguments."
